#!/bin/bash

if [ -z "$ACCESS_TOKEN" ] || [ -z "$USERNAME" ] || [ -z "$REGISTRY" ]
then
  echo "Docker information not provided"
  exit 1
fi

docker logout
echo "$ACCESS_TOKEN" | docker login $REGISTRY --username=$USERNAME --password-stdin