const valiadtionMiddleware = ({ schema }) => schema ? { name: "validation", params: { schema } } : null;

// Private -> Authentication Required
const authenticationMiddleware = ({ auth, private }) => {
  if (!auth) return null;

  return {
    name: "authentication",
    params: {
      private: private || false
    }
  }
};

// Protected -> Authorize
const authorizationMiddleware = ({ protected }) => !!protected ? { name: "authorization" } : null;

const prepareMiddleware = (middleware, funcs, extras) => {
  const middlewareUsed = middleware || [];

  // Apply Special Middleware FIRST -> Use in a regular way when ordering has to be custom
  const combinedMiddleware = [
    valiadtionMiddleware(extras),
    authenticationMiddleware(extras),
    authorizationMiddleware(extras),
    ...middlewareUsed,
  ];

  const preparedMiddleware = combinedMiddleware
    .filter(middleware => !!middleware)
    .map(({ name, params }) => funcs[name] ? funcs[name](params || {}) : null)

  return preparedMiddleware;
}

module.exports = { prepareMiddleware };
