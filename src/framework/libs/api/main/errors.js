const debug = require("debug");

const errorLogger = debug("app").extend("system").extend("error");

// Note: Next is needed for multiple levels of error handling (Multiple Error-Handlers) - Passing along (One after another)
const errorHandler = (error, req, res, next) => {
  const errorMessage = error.message || "Error occured";

  errorLogger("API-Error", errorMessage);

  return res.status(400).json({ message: errorMessage });
};

module.exports = { errorHandler };
