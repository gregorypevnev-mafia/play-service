const { Router } = require("express");
const { prepareMiddleware } = require("./middleware");
const { getMethod } = require("./methods");

const createRoutes = ({ path, middleware, use, ...extras }, middlewareFunctions, dependencies) => {
  const pathUsed = path || "/";
  const fullMiddleware = prepareMiddleware(middleware, middlewareFunctions, extras);

  const router = Router();

  // CANNOT USE MIDDLEWARE DIRECTLY ON ROUTER -> WILL BE USED REGARDLESS OF PATH-MATCHING

  // Checking if a controller is a recursive one - Either and Array or explicitly declares a nested controller
  if (Array.isArray(use) || use.use) {
    const controllers = Array.isArray(use) ? use : [use];

    return controllers.reduce(
      (router, config) => router.use(pathUsed, ...fullMiddleware, createRoutes(config, middlewareFunctions, dependencies)),
      router
    );
  }

  router[getMethod(use.method)](pathUsed, ...fullMiddleware, use.controller(dependencies));

  return router;
};

module.exports = { createRoutes };
