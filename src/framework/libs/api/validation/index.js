const defaultSchemas = require("./defaults");
const { createValidationMiddleware } = require("./middleware");

const mapSchemas = schemas => schemas.reduce((mapped, { name, schema }) => ({
  ...mapped,
  [name]: schema,
}), {});

const createValidation = schemas => {
  // Provide Defaults -> Allow overwriting
  const combinedSchemas = Object.assign(
    {},
    mapSchemas(defaultSchemas),
    mapSchemas(schemas)
  );

  return {
    middleware: [
      createValidationMiddleware(combinedSchemas)
    ],
  };
};

module.exports = { createValidation };