const yup = require("yup");

const filenamePattern = /^\w+\.\w+$/;

const fileUploadSchema = yup.object({
  file: yup.string().matches(filenamePattern).required("Provide name of the file"),
});

module.exports = {
  name: "file-upload",
  schema: fileUploadSchema,
};