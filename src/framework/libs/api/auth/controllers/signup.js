const createSignUpController = ({
  createUser,
  encodeToken,
}) => {
  const signup = ({ }) => async (req, res) => {
    try {
      const user = await Promise.resolve(createUser(req.body));

      const token = await Promise.resolve(encodeToken(user));

      return res.status(200).json({ user, token });
    } catch (error) {
      return res.status(400).json({ message: error.message });
    }
  };

  return {
    path: "/signup",
    schema: "signup",
    use: {
      method: "POST",
      controller: signup,
    }
  }
};

module.exports = { createSignUpController };
