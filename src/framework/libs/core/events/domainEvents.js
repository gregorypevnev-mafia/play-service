const { EventEmitter } = require("events");
const debug = require("debug");

const eventLogger = debug("app").extend("system").extend("events");

const createDomainEvents = (mq, validator) => {
  const listeners = []; // Global listeners
  const emitter = new EventEmitter();

  const notify = (type, payload) => {
    // Notify Specific listeners (Event-Hadles)
    emitter.emit(type, payload);

    // Notify Global Listeners (Message-Pipes)
    listeners.forEach(listener => listener({ type, payload }));
  }

  mq.register((type, payload) => {
    eventLogger("System-Event", type, payload);

    if (validator(type)) notify(type, payload);
  })

  return {
    // Listen to a specific event
    on(type, listener, errorListener = null) {
      emitter.on(type, async data => {
        try {
          eventLogger("Domain-Event", type, data);

          await listener(data);
        } catch (error) {
          console.log("EVENT-ERROR", type, error);
          // Note: Could additionally handle errors
          if (errorListener) errorListener(error);
        }
      });
    },
    // Listen to ALL Events
    register(listener) {
      listeners.push(listener);
    },
    async emit(type, payload) {
      if (!validator(type)) {
        eventLogger("Invalid Event", type);

        return;
      }

      eventLogger("Emitted", type, payload);

      notify(type, payload);

      // IMPORTANT: Use MQ before emitting the event
      //  - Handler might publish an Event itself -> OUT-OF-ORDER
      await mq.send(type, payload);
    }
  }
}

module.exports = { createDomainEvents };
