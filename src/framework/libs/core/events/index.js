const { createDomainEvents } = require("./domainEvents");
const { createKafka } = require("./kafka");
const { toTopic, extractTopics, isEventValid } = require("./utils");

const createKafkaDomainEvents = (events, kafkaConfig) => {
  const kafka = createKafka(kafkaConfig, extractTopics(events), toTopic(events));
  const domainEvents = createDomainEvents(kafka, isEventValid(events));

  return domainEvents;
};

module.exports = {
  createDomainEvents: createKafkaDomainEvents,
};
