const createDomainTasks = queue => {
  const tasks = {};
  const initialTasks = [];
  let initialized = false;

  const getTask = taskName => {
    const task = tasks[taskName];

    if (!task) throw { message: "Task is not registered" };

    return task;
  }

  return {
    register({ name, params, processor, auto }) {
      const task = queue.createTask(name, params, processor);

      if (auto) initialTasks.push(task);
      else tasks[name] = task;
    },
    async schedule(name, data, params = null) {
      await getTask(name).run(data, params);
    },
    async cancel(name, id) {
      await getTask(name).cancel(id);
    },
    initialize() {
      if (initialized) return;

      initialized = true;
      initialTasks.forEach(task => task.run());
    }
  }
}

module.exports = { createDomainTasks }; 