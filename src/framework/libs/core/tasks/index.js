const { createQueue } = require("./bull");
const { createDomainTasks } = require("./domainTasks");

const createBullDomainTasks = queueConfig => {
  const queue = createQueue(queueConfig);

  const domainTasks = createDomainTasks(queue);

  return domainTasks;
};

module.exports = {
  createDomainTasks: createBullDomainTasks,
};
