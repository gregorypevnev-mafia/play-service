const mongo = require("./mongo");
const redis = require("./redis");
const service = require("./service");
const sql = require("./sql");

module.exports = [
  mongo,
  redis,
  service,
  sql,
];
