const { MongoClient } = require("mongodb");
const debug = require("debug");

const mongoLogger = debug("app").extend("system").extend("mongo");

const AUTH_DB = "admin";

const formatURL = (host, port, user, password) => `mongodb://${user}:${password}@${host}:${port}/${AUTH_DB}`;

const createMongoClient = async ({ host, port, user, password, database, replicaSet }) => {
  const url = formatURL(host, port, user, password);

  mongoLogger("Connecting to (Authentication)", url);

  const client = await MongoClient.connect(url, {
    wtimeout: 1000,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    replicaSet, // Important: Redirects to Primary-Node using supplied settings -> Use "hosts" to redirect Primary-Node Host to "localhost"
  });

  mongoLogger("Using database", database, "for operation");

  const db = client.db(database);

  return {
    client,
    db,
  };
};

module.exports = { createMongoClient };
