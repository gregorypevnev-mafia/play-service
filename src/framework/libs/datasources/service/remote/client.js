const axios = require("axios").default;
const { logRequest, logResponse } = require("./logging");

const requestInterceptor = req => {
  logRequest(req);

  return req;
}

const responseInterceptor = res => {
  logResponse(res);

  return res;
};

const createRemoteClient = ({ url, timeout }, ) => {
  const client = axios.create({
    baseURL: url,
    timeout,
  });

  client.interceptors.request.use(requestInterceptor);

  client.interceptors.response.use(responseInterceptor);

  return client;
};

module.exports = { createRemoteClient };
