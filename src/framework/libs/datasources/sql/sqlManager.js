const { provisionOperations } = require("../common");

const createSQLManager = client => {
  let transaction = null;

  const provisionSQLOperations = (operations, isTransactional) => {
    if (!isTransactional) return provisionOperations(client, operations);

    const preparedOperations = Object.keys(operations).reduce(
      (transactionalOperations, operationName) => ({
        ...transactionalOperations,
        [operationName]: async (...args) => {
          if (transaction == null || transaction.isCompleted())
            transaction = await client.transaction();

          return operations[operationName](transaction)(...args);
        },
      }),
      {},
    );

    return {
      async commit() {
        await transaction.commit();
      },
      async rollback() {
        await transaction.rollback();
      },
      ...preparedOperations,
    }
  };

  return {
    provisionOperations: provisionSQLOperations,
  }
};

module.exports = { createSQLManager };
