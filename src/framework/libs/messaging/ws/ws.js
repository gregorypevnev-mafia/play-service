const WS = require("ws");
const urlUtil = require("url");

const getConnectionDetails = ({ fromQuery, fromTicket, }) => mode => query => {
  if (mode === "ticket") return fromTicket(query.ticket || "");

  if (mode === "query") return fromQuery(query);

  return null;
};

const createWebSocketServer = ({
  basePath,
  mode,
}, {
  fromQuery,
  fromTicket,
}, onConnection) => {
  const connectionDetails = getConnectionDetails({ fromQuery, fromTicket })(mode);

  const wss = new WS.Server({
    noServer: true,
    path: basePath,
  });

  wss.on("connection", onConnection);

  const upgrade = async (request, socket, head) => {
    const { query } = urlUtil.parse(request.url, true);

    // Generalize Connection-Handling - Does NOT necessary have to be tickets (Debugging / Public Channel / etc.)

    const result = await Promise.resolve(connectionDetails(query));

    if (!result) return socket.destroy();

    const { user, room } = result;

    wss.handleUpgrade(request, socket, head, ws => {
      // Triggering event on the Web-Socket-Server (Extends Event-Emitter) - NOT a Web-Socket Message
      wss.emit("connection", ws, {
        user,
        room,
      });
    });
  }

  return { upgrade };
}

module.exports = { createWebSocketServer };
