const ticketKey = ticket => `TICKET__${ticket}`;

const createTicketStore = (client, createTicket, { ttl }) => {
  const saveTicket = async data => {
    const { ticket, details } = await Promise.resolve(createTicket(data));

    await client.setex(ticketKey(ticket), ttl, JSON.stringify(details));

    return ticket;
  }

  const loadTicket = async ticket => {
    const details = await client.get(ticketKey(ticket));

    if (!details) return null;

    const data = JSON.parse(details);

    await client.del(ticketKey(ticket));

    return data;
  };

  return {
    saveTicket,
    loadTicket,
  }
};

module.exports = { createTicketStore };
