const { EventEmitter } = require("events");
const { createRedisClient } = require("../libs/datasources/redis/redisClient");
const { createMessenger } = require("../libs/messaging/messenger");
const { createTicketStore } = require("../libs/messaging/tickets");
const { createWebSocketServer } = require("../libs/messaging/ws");

const EVENT_NAME = "message";

const preparePipes = (pipes, dependencies) =>
  pipes.reduce((preparedPipes, { name, pipe }) => ({
    ...preparedPipes,
    [name]: pipe(dependencies),
  }), {});

const prepareMessagingPipes = ({ input, output }, dependencies) => ({
  input: preparePipes(input, dependencies),
  output: preparePipes(output, dependencies),
});

// Structure: Configuration (Config-Component), App (Provided from User), Dependencies (Provided from Loaders)

const loadMessenger = async ({
  channels: {
    messageChannel,
    controlChannel,
  },

  messages: {
    closeMessage,
    infoMessage,
  },

  events: {
    connectedEvent,
    disconnectedEvent
  },

  store: {
    port,
    host
  },

  tickets: {
    ttl,
  },

  ws: {
    basePath,
    mode,
  },
}, {
  connection: {
    createTicket,
    extractQueryData,
  },
  pipes: {
    input,
    output,
  }
}, dependencies) => {
  const emitter = new EventEmitter();

  // Read And Writes clients SPECIFICALLY for Web-Sockets
  const redisWriteClient = await createRedisClient({ port, host }, true);
  const redisReadClient = await createRedisClient({ port, host });

  const messenger = createMessenger({
    readClient: redisReadClient,
    writeClient: redisWriteClient,
  }, {
    pipes: prepareMessagingPipes({ input, output }, dependencies),
    events: { connectedEvent, disconnectedEvent }
  }, {
    channels: {
      messageChannel,
      controlChannel,
    },

    messages: {
      closeMessage,
      infoMessage,
    },
  }, ({ type, payload }) => {
    emitter.emit(EVENT_NAME, { type, payload });
  });

  const tickets = createTicketStore(redisWriteClient, createTicket, { ttl });

  const wss = createWebSocketServer({ basePath, mode }, {
    fromQuery: extractQueryData,
    fromTicket: tickets.loadTicket,
  }, (socket, { user, room }) => messenger.connect(socket, user, room));

  const registrator = {
    register(listener) {
      emitter.on(EVENT_NAME, listener);
    }
  };

  return {
    messenger,
    tickets,
    wss,
    registrator,
  };
};

module.exports = { loadMessenger };
