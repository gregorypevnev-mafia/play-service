const { microserviceBuilder } = require("./framework");

(async function () {
  const microservice = await microserviceBuilder()
    .withEvents(require("./app/events.json"))
    .addEventHandlers(require("./app/event-handlers"))
    .addRepositories(require("./app/repositories"))
    .addServices(require("./app/services"))
    .withTasks(require("./app/tasks"))
    .useInfo()
    .build();

  microservice.start();
}());
