const { MAFIA_ROLE, TOWN_RESULT, MAFIA_RESULT, PLAYER_DEAD } = require("./common/constants");
const {
  phaseDetails,
  startingPhase,
  nextStage
} = require("./common/stages");
const { processActions } = require("./common/actions");
const { countRoles } = require("./roles");

const STARTING_STEP = 1;

const initialStage = () => ({
  step: STARTING_STEP,
  phase: startingPhase(),
});

const next = ({ stage }) => nextStage(stage);

const isStandby = ({ phase }) => {
  const phaseData = phaseDetails(phase);

  return phaseData.standby;
}

const gameInfo = ({ id, details, step, phase }) => ({
  id,
  step,
  phase: phaseDetails(phase).title,
  ...details,
});

const phaseDuration = ({ phase }) => Number(phaseDetails(phase).duration);

// IMPORTANT: Separating Play-Phase and Standby-Phase for Play-Service
//  - NO Encapsulation
//  -> Allows determining whether passing Votes or Actions => Clearing Votes or Actions / Adding Action or 

const selectTarget = votes =>
  votes.reduce(({ max, counts }, target) => {
    counts[target] = (counts[target] || 0) + 1;

    return {
      max: (max === null || counts[target] > counts[max]) ? target : max,
      counts,
    }
  }, {
    max: null,
    counts: {},
  }).max;

// Returning an action
const processPlay = ({ phase }, votes) => {
  const phaseData = phaseDetails(phase);

  if (phaseData.standby) throw new Error("Not a Play Step");

  return {
    action: phaseData.action,
    target: selectTarget(votes),
  };
};

// Returning updated list of players
const processStandby = ({ phase }, actions) => {
  const phaseData = phaseDetails(phase);

  if (!phaseData.standby) throw new Error("Not a Standby Step");

  return processActions(actions);
};

const applyChange = (players, id, changedStatus) => {
  const targetPlayer = players[id];

  if (!targetPlayer) return players;

  return {
    ...players,
    [id]: {
      ...targetPlayer,
      status: changedStatus,
    }
  };
};

const applyChanges = (players, changes) =>
  Object.keys(changes || {}).reduce(
    (players, playerId) => applyChange(players, playerId, changes[playerId]),
    players
  );

const validateGame = ({ roles, players }) =>
  countRoles(roles) === players.length;

const gameResult = players => {
  const { town, mafia } = Object.values(players)
    .reduce(({ town, mafia }, { status, role }) => {
      if (status === PLAYER_DEAD) return { town, mafia };

      if (role === MAFIA_ROLE) return { town, mafia: mafia + 1 };

      return { mafia, town: town + 1 };
    }, { town: 0, mafia: 0 });

  if (mafia === 0) return TOWN_RESULT;
  if (town < mafia) return MAFIA_RESULT;
  return null;
};

const hasMafiaWon = ({ result }) =>
  result === MAFIA_RESULT;

const hasTownWon = ({ result }) =>
  result === TOWN_RESULT;

module.exports = {
  initialStage,
  gameInfo,
  phaseDuration,
  next,
  isStandby,
  processPlay,
  processStandby,
  applyChanges,
  validateGame,
  gameResult,
  hasMafiaWon,
  hasTownWon,
};
