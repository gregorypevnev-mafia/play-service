module.exports = [
    require('./votes'),
    require('./games'),
    require('./players'),
    require('./actions'),
    require('./details')
];