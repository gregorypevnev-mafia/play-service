const name = "games";
const datasource = "store";
const transactional = false;

const gameKey = gameId => `GAME_${gameId}`;

const load = redis => async gameId => {
  const data = await redis.get(gameKey(gameId));

  return JSON.parse(data);
};

const save = redis => async ({ id, ...data }) => {
  await redis.set(gameKey(id), JSON.stringify({ id, ...data }));
};

const remove = redis => async gameId => {
  await redis.del(gameKey(gameId));
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    load,
    save,
    remove,
  },
};