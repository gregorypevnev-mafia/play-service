const name = "players";
const datasource = "store";
const transactional = false;

// Using Set - No Duplicate players -> Preventing double connection

const playersKey = gameId => `PLAYERS_${gameId}`;

const load = redis => async gameId =>
  redis.smembers(playersKey(gameId));

const addPlayer = redis => async (gameId, playerId) => {
  await redis.sadd(playersKey(gameId), playerId);

  const newLength = await redis.scard(playersKey(gameId));

  return newLength;
};

const removePlayer = redis => async (gameId, playerId) => {
  await redis.srem(playersKey(gameId), playerId);

  const newLength = await redis.scard(playersKey(gameId));

  return newLength;
};

const clear = redis => async gameId => {
  await redis.del(playersKey(gameId));
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    load,
    addPlayer,
    removePlayer,
    clear,
  },
};
