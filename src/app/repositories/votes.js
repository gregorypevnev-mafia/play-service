const name = "votes";
const datasource = "store";
const transactional = false;

const votesKey = gameId => `VOTES_${gameId}`;

const votersKey = gameId => `VOTERS_${gameId}`;

// Votes - NOT a Set (Multiple Votes for the same player)

const loadVotes = redis => async gameId =>
  redis.lrange(votesKey(gameId), 0, -1);

const addVote = redis => async (gameId, targetId) => {
  // Push to back (From Right)
  await redis.rpush(votesKey(gameId), targetId);
};

const clearVotes = redis => async gameId => {
  await redis.del(votesKey(gameId));
};

// Voters - Set (Checking if a player has voted + No duplication)

const isVoter = redis => (gameId, voterId) =>
  redis.sismember(votersKey(gameId), voterId);

const addVoters = redis => async (gameId, voterIds) => {
  await redis.sadd(votersKey(gameId), voterIds);
};

const removeVoter = redis => async (gameId, voterId) => {
  await redis.srem(votersKey(gameId), voterId);
};

const clearVoters = redis => async gameId => {
  await redis.del(votersKey(gameId));
};

const votersCount = redis => async gameId =>
  await redis.scard(votersKey(gameId));

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    // Votes
    loadVotes,
    addVote,
    clearVotes,
    // Voters
    isVoter,
    addVoters,
    removeVoter,
    clearVoters,
    votersCount,
  },
};