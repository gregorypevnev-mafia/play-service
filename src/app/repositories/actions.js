const name = "actions";
const datasource = "store";
const transactional = false;

const actionsKey = gameId => `ACTIONS_${gameId}`;

const processActions = actions => actions.map(JSON.parse);

const load = redis => async gameId =>
  redis.lrange(actionsKey(gameId), 0, -1)
    .then(processActions);

const add = redis => async (gameId, action) => {
  // Push to back (From Right)
  await redis.rpush(actionsKey(gameId), JSON.stringify(action));
};

const clear = redis => async gameId => {
  await redis.del(actionsKey(gameId));
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    load,
    add,
    clear,
  },
};