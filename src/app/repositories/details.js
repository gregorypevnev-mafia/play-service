const name = "details";
const datasource = "games";
const transactional = false;

const findDetails = axios => gameId =>
  axios({
    url: `/api/games/${gameId}`,
    method: "GET",
  }).then(({ data }) => data.game);

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    findDetails
  },
};