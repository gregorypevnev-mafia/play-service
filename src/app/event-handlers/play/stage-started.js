const type = "stage-started";

const handler = ({
  tasks,
  events,
  services: { games: gamesService },
  repositories: { games: gamesRepo, votes: votesRepo },
}) => async ({ gameId }) => {
  const { stage, players } = await gamesRepo.load(gameId);

  const voters = gamesService.playersForPhase(stage, players);

  const isStandby = gamesService.isStandby({ stage });
  const phaseDuration = gamesService.phaseDuration(stage);

  if (voters.length === 0 || isStandby) {
    await events.emit("stage-ended", { gameId });

    return;
  }

  await votesRepo.addVoters(gameId, voters);

  await tasks.schedule(
    "next-stage",
    { id: gameId, gameId },
    { delay: phaseDuration }
  );
};

module.exports = {
  type,
  handler
}