const type = "player-voted";

const handler = ({
  tasks,
  events,
  repositories: { votes }
}) => async ({ gameId, playerId, targetId }) => {
  const isVoter = await votes.isVoter(gameId, playerId);

  if (!isVoter) return;

  await Promise.all([
    targetId ? votes.addVote(gameId, targetId) : null,
    votes.removeVoter(gameId, playerId),
  ]);

  const leftToVote = await votes.votersCount(gameId);

  if (leftToVote === 0) {
    await tasks.cancel("next-stage", gameId);

    await events.emit("stage-ended", { gameId });
  }
};

module.exports = {
  type,
  handler
}