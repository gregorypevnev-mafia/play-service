const type = "stage-ended";

// Note: Same logic as in "next-stage" (Sending Stage-Event to the Application + Task)
//  -> Find a way to simplify / reuse

const handler = ({
  events,
  services: {
    formatter,
    games: gamesService
  },
  repositories: {
    games: gamesRepo,
    actions: actionsRepo,
    votes: votesRepo
  }
}) => async ({ gameId }) => {
  const game = await gamesRepo.load(gameId);

  if (!game) return;

  const stage = gamesService.nextStage(game);

  if (gamesService.isStandby(game)) {
    const actions = await actionsRepo.load(gameId);

    const changes = gamesService.changes(game, actions);

    const players = gamesService.apply(game, changes);

    const result = gamesService.gameResult(players);

    // Game Finished -> Terminating
    if (result !== null) {
      return events.emit("game-finished", { gameId, result, changes, actions });
    }

    await Promise.all([
      actionsRepo.clear(gameId),
      gamesRepo.save({ ...game, stage, players }),
    ]);

    return events.emit("stage-started", formatter.stageData(gameId, stage, changes, actions));
  }

  const votes = await votesRepo.loadVotes(gameId);

  const newAction = gamesService.action(game, votes);

  await Promise.all([
    votesRepo.clearVotes(gameId),
    // To make sure they don't vote in the next stage
    votesRepo.clearVoters(gameId),
    newAction ? actionsRepo.add(gameId, newAction) : null,
    gamesRepo.save({ ...game, stage }),
  ]);

  events.emit("stage-started", formatter.stageData(gameId, stage));
};

module.exports = {
  type,
  handler
}