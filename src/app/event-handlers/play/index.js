module.exports = [
    require('./vote'),
    require('./stage-started'),
    require('./stage-ended'),
];