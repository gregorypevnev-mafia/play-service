const type = "player-left";

const handler = ({
  events,
  repositories: { games, players }
}) => async ({ gameId, playerId }) => {
  const game = await games.load(gameId);

  await Promise.all([
    players.removePlayer(gameId, playerId),
    games.save({ ...game, status: "PAUSED" })
  ]);

  events.emit("game-paused", { gameId });
};

module.exports = {
  type,
  handler
}