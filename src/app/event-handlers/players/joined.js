const type = "player-joined";

const handler = ({
  events,
  repositories: { games, players }
}) => async ({ gameId, playerId }) => {
  const [game, playersCount] = await Promise.all([
    games.load(gameId),
    players.addPlayer(gameId, playerId)
  ]);

  if (game.size === playersCount) {
    await games.save({ ...game, status: "RESUMED" });

    events.emit("game-resumed", { gameId });
  }
};

module.exports = {
  type,
  handler
}