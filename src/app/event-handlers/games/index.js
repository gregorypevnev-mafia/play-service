module.exports = [
    require('./started'),
    require('./finished'),
    require('./resumed'),
    require('./paused')
];