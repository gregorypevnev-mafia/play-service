const type = "game-resumed";

const handler = ({
  events,
  repositories: { games, votes },
  services: { formatter }
}) => async ({ gameId }) => {
  // Resetting the stage of current stage just in case
  // Note: Timer is reset by "paused" handler
  const [{ stage }, _, __] = await Promise.all([
    games.load(gameId),
    votes.clearVotes(gameId),
    votes.clearVoters(gameId),
  ]);

  events.emit("stage-started", formatter.stageData(gameId, stage));
};

module.exports = {
  type,
  handler
}