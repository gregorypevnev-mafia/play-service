const type = "game-paused";

const handler = ({ tasks }) => async ({ gameId }) => {
  await tasks.cancel("next-stage", gameId);
};

module.exports = {
  type,
  handler
}