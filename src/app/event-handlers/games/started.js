const type = "game-started";

const handler = ({
  repositories: { games, details }
}) => async ({ game: { id } }) => {
  const game = await details.findDetails(id);

  await games.save(game);
};

module.exports = {
  type,
  handler
}