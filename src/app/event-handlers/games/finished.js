const type = "game-finished";

const handler = ({
  repositories: { games, players, votes, actions }
}) => async ({ gameId }) => {
  await Promise.all([
    games.remove(gameId),
    players.clear(gameId),
    votes.clearVotes(gameId),
    votes.clearVoters(gameId),
    actions.clear(gameId),
  ]);
};

module.exports = {
  type,
  handler
}