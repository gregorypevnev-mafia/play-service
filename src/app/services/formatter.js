const name = "formatter";

const stageData = () => (id, stage, changes = {}, actions = []) => ({
  gameId: id,
  stage,
  changes: changes || {},
  actions: actions || [],
});

module.exports = {
  name,
  functions: {
    stageData,
  }
};