const { games: gameUtils, players: playerUtils } = require("../mafia-game");

const name = "games";

const isPlayable = () =>
  ({ stage }) => !gameUtils.isStandby(stage);

const isStandby = () =>
  ({ stage }) => gameUtils.isStandby(stage);

const action = () =>
  ({ stage }, votes) => (votes && votes.length > 0) ? gameUtils.processPlay(stage, votes) : null;

const changes = () =>
  ({ stage }, actions) => gameUtils.processStandby(stage, actions);

const apply = () =>
  ({ players }, changes) => gameUtils.applyChanges(players, changes);

const nextStage = () =>
  game => gameUtils.next(game);

const gameResult = () =>
  players => gameUtils.gameResult(players);

const playersForPhase = () =>
  (stage, players) => playerUtils.playersForPhase(stage, players).map(({ id }) => id);

const phaseDuration = () =>
  ({ phase }) => gameUtils.phaseDuration({ phase }) * 1000;

module.exports = {
  name,
  functions: {
    isPlayable,
    isStandby,
    action,
    changes,
    apply,
    nextStage,
    gameResult,
    playersForPhase,
    phaseDuration,
  }
};