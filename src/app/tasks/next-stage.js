const auto = false;

const name = "next-stage";

// FLUSHALL to delete tasks
const params = {
  delay: 5000,
};

const processor = ({ events }) => ({ gameId }) => {
  events.emit("stage-ended", { gameId });
};

module.exports = {
  auto,
  name,
  params,
  processor,
};
