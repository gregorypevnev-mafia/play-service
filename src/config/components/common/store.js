const DEFAULT_STORE_HOST = "localhost";
const STORE_HOST = String(process.env.STORE_HOST || DEFAULT_STORE_HOST);

const DEFAULT_STORE_PORT = 6000;
const STORE_PORT = String(process.env.STORE_PORT || DEFAULT_STORE_PORT);

const DEFAULT_STORE_PASSWORD = "pass";
const STORE_PASSWORD = String(process.env.STORE_PASSWORD || DEFAULT_STORE_PASSWORD);

module.exports = {
  host: STORE_HOST,
  port: STORE_PORT,
  password: STORE_PASSWORD
};
